from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import MultiFieldPanel, FieldPanel, InlinePanel

from modelcluster.fields import ParentalKey

from amecron.utils.models import HeroImage, Preview


class MajorEndeavor(Orderable, Preview):
    page = ParentalKey(
        'Endeavor', related_name='endeavor'
    )


class Endeavor(HeroImage, Page):
    intro = RichTextField()
    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        MultiFieldPanel(
            [
                InlinePanel(
                    'endeavor',
                    panels=MajorEndeavor.panels,
                )
            ],
            heading="Areas of Endeavor",
            classname="collapsible"
        )
    ]

    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            HeroImage.panels,
            heading="Hero Section",
            classname="collapsible"
        )
    ]
