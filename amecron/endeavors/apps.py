from django.apps import AppConfig


class EndeavorsConfig(AppConfig):
    name = 'amecron.endeavors'
