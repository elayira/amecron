from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = 'amecron.utils'
    verbose_name = 'Utils'
