from django.db import models

from wagtail.wagtailadmin.edit_handlers import (
    FieldPanel, MultiFieldPanel, PageChooserPanel
)
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtailgmaps.edit_handlers import MapFieldPanel


class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
    ]

    class Meta:
        abstract = True


class ContactFields(models.Model):
    office_title = models.CharField(max_length=255, blank=True)
    telephone = models.CharField(max_length=20, blank=True)
    email = models.EmailField(blank=True)
    address = models.CharField(max_length=255, blank=True)
    post_code = models.CharField(max_length=10, blank=True)

    facebook = models.ForeignKey(
        'corporateprofile.SocialMediaLink', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    linkedin = models.ForeignKey(
        'corporateprofile.SocialMediaLink', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )

    twitter = models.ForeignKey(
        'corporateprofile.SocialMediaLink', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )

    panels = [
        FieldPanel('office_title',
                   'The full/formatted name of the person or organisation'),
        FieldPanel('telephone'),
        FieldPanel('email'),
        MapFieldPanel('address'),
        FieldPanel('post_code'),
        SnippetChooserPanel('facebook'),
        SnippetChooserPanel('linkedin'),
        SnippetChooserPanel('twitter'),
    ]

    class Meta:
        abstract = True


# Carousel items

class CarouselItem(LinkFields):
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    embed_url = models.URLField("Embed URL", blank=True)
    caption = RichTextField(blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('embed_url'),
        FieldPanel('caption'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True


class HeroImage(models.Model):
    hero_image = models.ForeignKey(
        'wagtailimages.Image', related_name="+", on_delete=models.SET_NULL,
        null=True, blank=True, verbose_name='Image'
    )
    hero_image_caption = models.CharField(max_length=50, blank=True, verbose_name='caption')

    panels = [
        ImageChooserPanel('hero_image'),
        FieldPanel('hero_image_caption'),
    ]

    class Meta:
        abstract = True


class Preview(LinkFields):
    name = models.CharField(max_length=255, default='')
    position = models.CharField(max_length=255, default='', blank=True)
    bio = models.TextField(default='')
    image = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.SET_NULL, null=True, blank=True, related_name='+'
    )

    panels = [
        FieldPanel('name'),
        FieldPanel('position'),
        FieldPanel('bio'),
        ImageChooserPanel('image'),
        MultiFieldPanel(LinkFields.panels, "More"),
    ]

    class Meta:
        abstract = True


class Sections(models.Model):
    title = models.CharField(max_length=255, default='')
    body = RichTextField(default='')

    panels = [
        FieldPanel('title'),
        FieldPanel('body', classname="full"),
    ]

    class Meta:
        abstract = True
