from wagtail.wagtailcore.fields import RichTextField

from wagtail.wagtailadmin.edit_handlers import (
    FieldPanel, MultiFieldPanel, InlinePanel
)

from wagtail.wagtailcore.models import Orderable
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField
from modelcluster.fields import ParentalKey
from amecron.utils.models import ContactFields, HeroImage


class FormField(AbstractFormField):
    page = ParentalKey('contact.FormPage', related_name='form_fields')


class FormPage(AbstractEmailForm):
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)


FormPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('intro', classname="full"),
    InlinePanel('form_fields', label="Form fields"),
    FieldPanel('thank_you_text', classname="full"),

    MultiFieldPanel([
        FieldPanel('to_address', classname="full"),
        FieldPanel('from_address', classname="full"),
        FieldPanel('subject', classname="full"),
    ], "Email")
]


class ContactFormField(AbstractFormField):
    page = ParentalKey('contact.Contact', related_name='form_fields')


class Office(Orderable, ContactFields):
    page = ParentalKey("contact.Contact", related_name="office_contact")


class Contact(AbstractEmailForm, HeroImage):
    thank_you_text = RichTextField(blank=True)
    intro = RichTextField(blank=True)

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('intro', classname="full"),
            ],
            heading="Introduction",
            classname="collapsible"
        ),


        MultiFieldPanel(
            [
                InlinePanel('office_contact'),
            ],
            heading="Contact Office",
            classname="collapsible"
        ),

        MultiFieldPanel(
            [
                InlinePanel('form_fields', label="Contact Form fields"),
                FieldPanel('thank_you_text', classname="full"),
            ],
            heading="Form Fields",
            classname="collapsible"
        ),

        MultiFieldPanel(
            [
                FieldPanel('to_address', classname="full"),
                FieldPanel('from_address', classname="full"),
                FieldPanel('subject', classname="full"),
            ],
            heading="Form Submission Response",
            classname="collapsible"
        ),
    ]

    promote_panels = AbstractEmailForm.promote_panels + [
        MultiFieldPanel(
            HeroImage.panels,
            heading="Hero Image",
            classname="collapsible"
        )
    ]
