from django.db import models

from modelcluster.models import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from amecron.corporateprofile.models import Personnel
from amecron.utils.models import HeroImage, ContactFields


class Blurb(Orderable):
    page = ParentalKey(
        'Home', related_name='blurb'
    )
    title = models.CharField(max_length=255, default='')
    image = models.ForeignKey(
        'wagtailimages.Image', related_name="blurb_image", on_delete=models.SET_NULL,
        null=True, verbose_name='Image'
    )
    quote = models.CharField(max_length=255, default='')
    body = models.TextField(default='')
    panel = [
        ImageChooserPanel('image'),
        FieldPanel('title', classname="title"),
        FieldPanel('quote', classname="full"),
        FieldPanel('body', classname="full")
    ]


class Team(Orderable, Personnel):
    page = ParentalKey(
        'Home', related_name='home_team'
    )


class Office(Orderable, ContactFields):
    page = ParentalKey("Home", related_name="main_office_contact")


class Home(HeroImage, Page):
    body = RichTextField(blank=True)
    intro = RichTextField(default='')
    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            HeroImage.panels,
            heading="Hero Image",
            classname="collapsible"
        ),
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        MultiFieldPanel(
            [
                InlinePanel(
                    'blurb',
                    panels=Blurb.panel,
                ),
            ],
            heading='Profile blurbs',
            classname="collapsible"
        ),
        MultiFieldPanel([
            InlinePanel(
                'home_team',
                panels=Team.panel,
            ),
        ],
            heading='Team Profile',
            classname="collapsible"
        ),
        MultiFieldPanel(
            [
                InlinePanel(
                    'main_office_contact',
                    panels=ContactFields.panels,
                )
            ],
            heading='Main Contact Info',
            classname="collapsible"
        ),

    ]

    class Meta:
        verbose_name = 'Home'
