from django.apps import AppConfig


class AboutConfig(AppConfig):
    name = "amecron.about"
    verbose_name = 'About'

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
