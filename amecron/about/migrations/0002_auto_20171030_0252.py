# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2017-10-30 01:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('corporateprofile', '0001_initial'),
        ('wagtailimages', '0019_delete_filter'),
        ('about', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('personnel_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='corporateprofile.Personnel')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='home_team', to='about.AboutHome')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
            bases=('corporateprofile.personnel', models.Model),
        ),
        migrations.AddField(
            model_name='homethumbnail',
            name='image',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='nail_image', to='wagtailimages.Image', verbose_name='Image'),
        ),
        migrations.AddField(
            model_name='homethumbnail',
            name='page',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='home_thumbnail', to='about.AboutHome'),
        ),
        migrations.AddField(
            model_name='abouthome',
            name='hero_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image', verbose_name='Image'),
        ),
    ]
