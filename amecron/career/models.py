from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Orderable
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField

from amecron.utils.models import HeroImage, Sections, ContactFields


class CareerSection(Orderable, Sections):
    page = ParentalKey("career.Career", related_name="career_sections")


class Office(Orderable, ContactFields):
    page = ParentalKey("career.Career", related_name="career_contact")


class CareerFormField(AbstractFormField):
    page = ParentalKey('career.Career', related_name='form_fields')


class Career(AbstractEmailForm, HeroImage):
    thank_you_text = RichTextField(blank=True)
    intro = RichTextField(blank=True)

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('intro', classname="full"),
            ],
            heading="Introduction",
            classname="collapsible"
        ),

        MultiFieldPanel(
            [
                InlinePanel('career_sections'),
            ],
            heading="Career Sections",
            classname="collapsible"
        ),
        MultiFieldPanel(
            [
                InlinePanel('career_contact'),
            ],
            heading="Contact Office",
            classname="collapsible"
        ),

        MultiFieldPanel(
            [
                InlinePanel('form_fields', label="Contact Form fields"),
                FieldPanel('thank_you_text', classname="full"),
            ],
            heading="Form Fields",
            classname="collapsible"
        ),

        MultiFieldPanel(
            [
                FieldPanel('to_address', classname="full"),
                FieldPanel('from_address', classname="full"),
                FieldPanel('subject', classname="full"),
            ],
            heading="Form Submission Response",
            classname="collapsible"
        ),
    ]

    promote_panels = AbstractEmailForm.promote_panels + [
        MultiFieldPanel(
            HeroImage.panels,
            heading="Hero Image",
            classname="collapsible"
        )
    ]

